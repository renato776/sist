package main

import (
	"log"
	"fmt"
	"net/http"

	"encoding/json"
	"github.com/julienschmidt/httprouter"

  "github.com/gocql/gocql"
)

type Banco struct {
	Abrv string `json:"abrv"`
	Name string `json:"name"`
}

type ErrorResponse struct {
	Code int `json:"code"`
	Message string `json:"message"`
}
var Cass *gocql.Session

func answer (w http.ResponseWriter, code int, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(data)
}
func chat (w http.ResponseWriter, message string) {
	w.Header().Set("Content-Type", "text/plain;charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, message)
}
func GracefullyFail(w http.ResponseWriter, msg string){
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusBadRequest)
	_ = json.NewEncoder(w).Encode(ErrorResponse{http.StatusBadRequest,msg})
}
func Hi(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
  if Cass == nil {
    GracefullyFail(w,"Connection to cluster got lost!")
    return
  }
  q := `select * from consulta4;`
  var bancos []Banco
  bancos = []Banco{}
  iter := Cass.Query(q).Iter()
  var a string
  var b string
  for iter.Scan(&a, &b){
    bancos = append(bancos, Banco{Abrv: a, Name: b})
  }
  if err:= iter.Close(); err != nil {
    GracefullyFail(w,fmt.Sprintf("%w", err))
    return
  }
  answer(w, http.StatusOK, bancos)
}

func OptionsReq(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS,POST,GET")
	w.Header().Set("Allow", "OPTIONS,POST,GET")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusNoContent)
}

func main(){
  var shadow error
  cluster := gocql.NewCluster("localhost")
  cluster.Keyspace = "sist"
  cluster.Consistency = gocql.One
  Cass,shadow = cluster.CreateSession()
  defer Cass.Close()

  if shadow != nil {
		panic(fmt.Errorf("Failed to open Cassandra cluster: %w", shadow))
  }
	router := httprouter.New()
	router.GET("/hello", Hi)
  router.OPTIONS("/hello", OptionsReq)
  fmt.Println("Server ready on port 1776")
	log.Fatal(http.ListenAndServe(":1776", router))
}
