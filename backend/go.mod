module gitlab.com/renato776/sist

go 1.15

require (
	github.com/gocql/gocql v0.0.0-20210504150947-558dfae50b5d
	github.com/julienschmidt/httprouter v1.3.0
)
