import axios from 'axios';

let url = process.env.VUE_APP_HTTP;

export default {

    consultar(id) {
        return axios.get(url + '/consulta/'+id);
    },

    pay(origin, target, amount) {
        return axios.post(url + '/pay', {
            source: origin,
            target: target,
            amount: amount
        });
    }
}
